This folder contains all the code for the UCB algorithm used during the experiment.

The main structure of this folder is: 
```
.   
├── Communication               # Folder containing the .py file which is the TCP client for sending and receiving data between the Python client and the Unity server during the experiment.
├── Algorithms
│   └── UCB_1_param.py           # The python script paired with RLExperiment.py. It contains the UCB algorithm looking for the optimal distortion gain.
│   
├── Experiment          
│   └── RLExperiment.py         # The script to run after having started the Unity project to find with the optimal distortion.
├── Logger                      # Folder containing the .py file that logs the logs explained below.
├── ExportData                  # Folder containing the .csv files that logs the progress of the experiment and the reaction of the subjects. There is a file left as an example.
├── Models                      # Folder containing the .json files that are backup of the Logger and the UCB models. It is to prevent that, if the experiment is interrupted due to a bug/any other reasons, we can load these files directly to avoid making the subject restart from the beginning. There is a file left as an example.
└── README.md                   # This file
```